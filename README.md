# ics-ans-role-node-exporter

Ansible role to install prometheus node-exporter.

## Role Variables

```yaml
node_exporter_version: 0.18.1
node_exporter_textfile_dir: /var/lib/node_exporter
node_exporter_base_dir: /opt/node_exporter
node_exporter_unzip_dir: "{{ node_exporter_base_dir }}/node_exporter-{{ node_exporter_version }}.linux-{{ node_exporter_architecture }}"
node_exporter_baseurl: "https://artifactory.esss.lu.se/artifactory/swi-pkg/prometheus/node_exporter"
node_exporter_download_url: "{{ node_exporter_baseurl }}/node_exporter-{{ node_exporter_version }}.linux-{{ node_exporter_architecture }}.tar.gz"
node_exporter_user: node_exporter
node_exporter_group: node_exporter

node_exporter_web_listen_address: "0.0.0.0:9100"

node_exporter_architecture: amd64

node_exporter_enabled_collectors:
  - ntp
  - systemd
  - textfile:
      directory: "{{ node_exporter_textfile_dir }}"
  - filesystem:
      ignored-mount-points: "^/(sys|proc|dev|tmpfs)($|/)"
      ignored-fs-types: "^(sys|proc|auto)fs$"

node_exporter_disabled_collectors: []

node_exporter_zfs_group_name: zfs
node_exporter_ldapservers_group_name: ldapservers
node_exporter_ldapsreplica_group_name: ldap_replicas
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-node-exporter
```

## License

BSD 2-clause
